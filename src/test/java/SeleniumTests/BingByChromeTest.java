package SeleniumTests;

import org.junit.After;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class BingByChromeTest {

    private static WebDriver driver;
    private static final String BING_URL = "https://www.bing.com/";


    @Test
    public void navigateToBingByChrome(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Desktop\\Drivers\\New Chrome\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(BING_URL);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS) ;
        driver.manage().window().maximize();

        WebElement acceptButton = driver.findElement(By.id("bnp_btn_accept"));
        acceptButton.click();

        WebElement searchTextBox = driver.findElement(By.id("sb_form_q"));
        searchTextBox.sendKeys("Telerik Academy Alpha");

        WebElement searchButton = driver.findElement(By.id("search_icon"));

        new Actions(driver).moveToElement(searchButton)
                .click()
                .perform();

        String actualTitle = driver.findElement(By.xpath("//*[@id='b_results']/li[1]/div[1]/h2/a")).getText();
        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";

        Assert.assertEquals(actualTitle, expectedTitle);

        driver.quit();
    }
}
