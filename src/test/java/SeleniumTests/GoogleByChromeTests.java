package SeleniumTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GoogleByChromeTests {

    private WebDriver driver;
    private static final String GOOGLE_URL = "https://www.google.com/";


    @Test
    public void navigateToGoogleByChrome() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Desktop\\Drivers\\New Chrome\\chromedriver.exe");

        driver = new ChromeDriver();
        driver.get(GOOGLE_URL);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS) ;

        WebElement agreeButton = driver.findElement(By.xpath("//button[@id='L2AGLb']"));
        agreeButton.click();

        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Telerik Academy Alpha");

        WebElement suggestionsSelection = driver.findElement(By.className("UUbT9"));
        WebElement searchButton = suggestionsSelection.findElement(By.name("btnK"));

        new Actions(driver).moveToElement(searchButton)
                .click()
                .perform();

        driver.manage().window().maximize();
        String actualTitle = driver.findElement(By.xpath("//*[@id='rso']/div[1]/div/div[1]/div/a/h3")).getText();
        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";

        Assert.assertEquals(actualTitle, expectedTitle);

        driver.quit();
    }
}
