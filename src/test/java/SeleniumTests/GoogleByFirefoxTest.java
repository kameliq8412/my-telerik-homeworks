package SeleniumTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class GoogleByFirefoxTest {

    private WebDriver driver;

    private static final String GOOGLE_URL = "https://www.google.com/";

    @Test
    public void navigateToGoogleByFirefox() {

        System.setProperty("webdriver.gecko.driver", "C:\\Users\\user\\Desktop\\Drivers\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get(GOOGLE_URL);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS) ;

        WebElement agreeButton = driver.findElement(By.id("L2AGLb"));
        agreeButton.click();

        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Telerik Academy Alpha");

        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]"));

        new Actions(driver).moveToElement(searchButton)
                .click()
                .perform();

        driver.manage().window().maximize();
        String actualTitle = driver.findElement(By.xpath("/html/body/div[7]/div/div[10]/div/div[2]/div[2]/div/div/div[1]/div/div[1]/div/a/h3")).getText();
        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";

        Assert.assertEquals(actualTitle, expectedTitle);

        driver.quit();
    }
}
